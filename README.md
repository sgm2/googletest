git clone https://github.com/google/googletest.git

g++ -std=c++11 -isystem googletest/googletest/include -Igoogletest/googletest -pthread -c googletest/googletest/src/gtest-all.cc

g++ -std=c++11 -isystem googletest/googletest/include -Igoogletest/googletest -pthread -c googletest/googletest/src/gtest_main.cc

ar -rv libgtest.a gtest_main.o gtest-all.o

g++ -std=c++11 -c func.cpp

g++ -std=c++11 -isystem googletest/googletest/include -c test.cpp

g++ -o test_add_one func.o test.o -L. -lgtest
