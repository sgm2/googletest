#include <gtest/gtest.h>
#include "func.h"

TEST(AddTest, Zero)
{
    EXPECT_EQ(3, add_one(1, 2));
}

TEST(AddTest, One)
{
    EXPECT_EQ(4, add_one(1, 2));
}
